# GitHub Markdown README Generator

## 📄 Description

A very simple &amp; quick way to generate outstanding README files for your next GitHub project 🚀

The generated README files are highly **optimized for SEO** and **eye-catching for your readers** 🥳

## 🛠 Installation

If you edit the script on your machine, you will have to run `npm link` to locally _symlink_ the package, and then run `github-readme-generator` in your terminal.

## 🎮 Usage

```bash
npx github-readme-generator
```

![GitHub README File Generator](github-readme-generator-usage.png)

## 😋 Who cooked it?

[![Pierre-Henry Soria](https://avatars0.githubusercontent.com/u/1325411?s=200)](https://ph7.me 'My personal website :-)')

[![@phenrysay][twitter-image]](https://twitter.com/phenrysay) [![pH-7][github-image]](https://github.com/pH-7)

**[Pierre-Henry Soria](https://ph7.me)**, a highly pragmatic passionate, zen &amp; cool software engineer 😊

## ⚖️ License

Generously distributed under the _MIT_

<!-- GitHub's Markdown reference links -->

[twitter-image]: https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white
[github-image]: https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white
